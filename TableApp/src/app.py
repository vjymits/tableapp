import json
from util import msg
from table_jobs import CreateTable, GetTableConfig, UpdateTable, DeleteTable
from row_jobs import CreateRow, GetRow, UpdateRow, DeleteRow


# TODO Add the logs
# TODO update, delete, list rows
# Validator is not working
# A error in custom name

jobs = {
    "POST::/api/v1/tables": CreateTable,
    "PUT::/api/v1/tables/{id}": UpdateTable,
    "GET::/api/v1/tables/{id}": GetTableConfig,
    "DELETE::/api/v1/tables/{id}": DeleteTable,

    "POST::/api/v1/rows": CreateRow,
    "GET::/api/v1/rows/{id}": GetRow,
    "PUT::/api/v1/rows/{id}": UpdateRow,
    "DELETE::/api/v1/rows/{id}": DeleteRow,
}


class Executer:

    def __init__(self, operation, event, context):
        self.operation = operation
        self.event = event

    def execute(self, validate=True):
        try:
            print("Executing opertion: " + self.operation)
            clz = jobs.get(self.operation)
            if clz is None:
                return 400, {"error": msg("OPERATION_NOT_REGISTERED")}
            job = clz()
            status, res = job.doit(self.event)
        except Exception as e:
            print(e)
            res = {"error": "Unknown Error."}
            status = 500
        return status, res


def lambda_handler(event, context):
    operation = find_operation(event)
    exe = Executer(operation, event, context)
    status, data = exe.execute()
    return {
        "statusCode": status,
        "body": json.dumps(data),
    }


def find_operation(event):
    method = event.get('httpMethod')
    resource = event.get('resource')
    operation = str(method) + '::' + str(resource)
    return operation
