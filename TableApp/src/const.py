

#API
BASE_URL = "/api/v1/"

#TABLE
OPEARTION_CREATE_TABLE =  "POST::/api/v1/tables"
OPERATION_GET_TABLE_CONFIG = "GET::/api/v1/tables/{id}"
OPERATION_UPDATE_TABLE = "PUT::/api/v1/tables/{id}"
OPERATION_DELETE_TABLE = "DELETE::/api/v1/tables/{id}"
#ROW
OPERATION_LIST_ROWS_BY_TABLE_ID = "GET::/api/v1/rows?tableId={tableId}"
OPERATION_ADD_ROW = "POST::/api/v1/rows"
OPERATION_GET_ROW = "GET::/api/v1/rows/{id}"
OPERATION_UPDATE_ROW = "PUT::/api/v1/rows/{id}"
OPERATION_DELETE_ROW = "DELETE::/api/v1/rows/{id}"
#schema
schema = {
        "POST::/api/v1/tables": {
            "name": {
                "key":"name",
                "required":True,
                "type":str,
                "min_length": 2,
                "max_length": 64
            },
            "custom-unique-name":{
                "key": "custom-unique-name",
                "required":False,
                "type":str,
                "min_length": 2,
                "max_length": 64
            },
            "description": {
                "key": "description",
                "required":False,                       
                "max_length": 256
            },
            "password":{
                "key": "password",
                "required":False,
                "max_length": 32
            },
            "columns":{
                "key": "columns",
                "required":True,
                "type":list,
                "min_length": 1,
                "max_length": 30
            }
        },
        "PUT::/api/v1/tables/{id}": {
            "name": {
                "key":"name",
                "required":True,
                "type":str,
                "min_length": 2,
                "max_length": 64
            },
            "custom-unique-name":{
                "key": "custom-unique-name",
                "required":False,
                "type":str,
                "min_length": 2,
                "max_length": 64
            },
            "description": {
                "key": "description",
                "required":False,                       
                "max_length": 256
            },
            "password":{
                "key": "password",
                "required":False,
                "max_length": 32
            },
            "columns":{
                "key": "columns",
                "required":True,
                "type":list,
                "min_length": 1,
                "max_length": 30
            }
        }        
    }

