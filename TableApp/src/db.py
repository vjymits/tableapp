import boto3
import time


def _prepare_row_update(row, columns):
    attr_values = {}
    attr_names = {}
    exp = 'set '

    def clean(x): return x.replace(" ", "")

    for col in columns:
        k = col.get('key')

        if k is None:
            continue
        val_key = ':'+clean(k)
        name_key = '#'+clean(k)

        attr_values[val_key] = row.get(k)
        attr_names[name_key] = k
        exp += ' '+name_key+'='+val_key+','
        print("namekey: " + name_key + ' valkey: ' + val_key+' exp as of now: '+exp)
    exp = exp[:-1]
    return {'names': attr_names, 'values': attr_values, 'exp': exp}


class DB:

    def __init__(self, table_name):
        self.table_name = table_name
        dynamodb = boto3.resource('dynamodb')
        self.table = dynamodb.Table(self.table_name)
        self.mp = ""

    """
        Get the table config
    """

    def get_table_config_by_id(self, id):
        pk = id
        sk = "config"
        res = self.table.get_item(
            Key={'pk': pk, "sk": sk},
            ProjectionExpression="#n, #d, #c, pk, createdAt",
            ExpressionAttributeNames={'#n': 'name', '#d': 'description', '#c': 'columns'}
        )
        return res

    def get_table_config_item(self, uid):
        res = self.get_table_config_by_id(uid)
        item = res.get("Item")
        return item

    def is_table_name_unique(self, unique_name):
        item = self.get_table_config_item("T:" + unique_name)
        if item is None:
            return True
        return False

    def create_table(self, uid, data):
        item = {
            "pk": "T:" + str(uid),
            "sk": "config",
            "name": data.get("name"),
            "description": data.get("description"),
            "password": data.get("password"),
            "columns": data.get("columns"),
            "email": data.get("email"),
            "createdAt": str(time.time())
        }
        print("Creating table with DB: " + str(item))
        res = self.table.put_item(Item=item)
        return res, item

    def update_table(self, uid, data):
        update_time = str(time.time())
        res = self.table.update_item(
            Key={"pk": uid, "sk": "config"},
            UpdateExpression="set #n=:n, #d=:desc,"
                             + "password=:pwd, #c=:cols, email=:eml, updatedAt=:up",
            ExpressionAttributeValues={
                ':n': data.get("name"),
                ':desc': data.get("description"),
                ':pwd': data.get("password"),
                ':cols': data.get("columns"),
                ':eml': data.get("email"),
                ':up': update_time
            },
            ExpressionAttributeNames={
                "#n": "name",
                "#d": "description",
                "#c": "columns"
            },
            ReturnValues="ALL_NEW"
        )
        data['updatedAt'] = update_time
        data['pk'] = uid
        return res, data

    def delete_table(self, uid, sk="config"):
        return self.table.delete_item(Key={"pk": uid, "sk": sk})

    def create_row(self, table_id, row_id, data):
        item = data
        item['pk'] = table_id
        item['sk'] = row_id
        print("Creating row in tableId: " + str(table_id))
        res = self.table.put_item(Item=item)
        return res, item

    def update_row(self, table_id, row_id, row, columns):
        update_time = str(time.time())
        row["updatedAt"] = update_time
        print("Going ahead to update row: ")
        result = _prepare_row_update(row, columns)
        key_element = {"pk": table_id, "sk": row_id}
        print("expression: " + result['exp'])
        print("attrvals: " + str(result['values']))
        print("attrnames: " + str(result['names']))
        res = self.table.update_item(Key=key_element, UpdateExpression=result['exp'],
                                     ExpressionAttributeValues=result['values'],
                                     ExpressionAttributeNames=result['names'],
                                     ReturnValues="ALL_NEW"
                                     )
        print("updated row response: " + str(res))
        return res, row

    def delete_row(self, table_id, row_id):
        return self.table.delete_item(Key={"pk": table_id, "sk": row_id})

    def get_row(self, table_id, row_id, item_only=False):
        pk = table_id
        sk = row_id
        res = self.table.get_item(
            Key={'pk': pk, "sk": sk}
        )
        if item_only:
            return res.get('Item')
        else:
            return res
