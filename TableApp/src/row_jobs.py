from db import DB
from validator import Validator
from util import ValidationError
from const import OPERATION_ADD_ROW, OPERATION_UPDATE_ROW, OPERATION_DELETE_ROW, OPERATION_GET_ROW
from util import convert_str_to_type, decorate_row_response, msg
import json
import uuid


def get_row_id(payload):
    pp = payload.get("pathParameters")
    v = Validator(required=True, type=str)
    row_id = pp.get("id")
    v.validate(row_id)
    return row_id


def split_ids(row_id):
    l = row_id.split('@')
    return l[0], l[1]


class RowJob:

    def __init__(self, operation):
        self.db = DB("TableAppDB")
        self.operation = operation

    def doit(self, payload):
        # Implement in all childs
        return 200, {"error": "Operation not implemented."}

    def validate(self, body, config=None):
        table_id = body.get('tableId')
        if not table_id:
            raise ValidationError("Valid tableId needs to pass.")
        if not config:
            config = self.db.get_table_config_item(table_id)
        if config is None:
            raise ValidationError("No such table found.")
        columns = config.get('columns')
        row = body.get('row')
        validated_row={}
        for column in columns:
            print("column schema: " + str(column))
            v = Validator(required=column.get('required', False),
                          type=convert_str_to_type(column.get('type', 'str')),
                          min_length=int(column.get('minLength', 0)),
                          max_length=int(column.get('maxLength', 32)))
            k = column.get('key')
            val = row.get(k)
            print("validating key: " + str(k) + " value: " + str(val) + " schema: " + str(column))
            status, msg = v.validate(val)
            validated_row[k]=val
            # Check the validation status
            if not status:
                # Raise since it fails
                print("Validation failed for key: " + k + " msg: " + str(msg))
                raise ValidationError(k + ": " + msg)
        body['row'] = validated_row


class CreateRow(RowJob):

    def __init__(self):
        super(CreateRow, self).__init__(OPERATION_ADD_ROW)
        self.body = None

    """
    POST method which create row 
    """

    def doit(self, payload):
        print("Creating new Row.")
        body = payload.get('body')
        body = json.loads(body)
        try:
            # TODO: get table config in bearer token
            self.validate(body)
            table_id = body.get('tableId')
            uid = 'R:'+str(uuid.uuid4())
            print("Row Id generated: " + str(uid))
            res, req = self.db.create_row(table_id, uid, body.get('row'))
            status = res["ResponseMetadata"]["HTTPStatusCode"]
            print("Row creation status : " + str(status))
            res['id']= uid+'@'+table_id
            return status, decorate_row_response(res, status)
        except ValidationError as e:
            return 400, {"error": e.message}


class UpdateRow(RowJob):
    def __init__(self):
        super(UpdateRow, self).__init__(OPERATION_UPDATE_ROW)
        self.body = None

    def doit(self, payload):
        body = payload.get('body')
        body = json.loads(body)
        try:
            row_id = get_row_id(payload)
            row_id, table_id = split_ids(row_id)
            body['tableId'] = table_id
            # TODO send the config in bearer token
            config = self.db.get_table_config_item(table_id)
            self.validate(body, config)
            row = body.get('row')
            res, row = self.db.update_row(table_id, row_id, row, config.get('columns'))
            status = res["ResponseMetadata"]["HTTPStatusCode"]
            print("Row update status : " + str(status) + " response: " + str(res))
            attrs = res["Attributes"]
            return status, decorate_row_response(attrs, status)
        except ValidationError as e:
            return 400, {"error": e.message}


class GetRow(RowJob):
    def __init__(self):
        super(GetRow, self).__init__(OPERATION_GET_ROW)
        self.body = None

    def doit(self, payload):
        try:
            row_id = get_row_id(payload)
            print("rowId: "+str(row_id))
            row_id, table_id = split_ids(row_id)
            res = self.db.get_row(table_id, row_id)
            print("fetched row: "+str(res))
            st = res['ResponseMetadata']['HTTPStatusCode']
            item = res.get('Item')
            if item is None:
                return 404, {"error": msg("NOT_FOUND")}
            return st, decorate_row_response(item, st)
        except Exception as e:
            return 400, {"error": "Failed to get row."}


class DeleteRow(RowJob):
    def __init__(self):
        super(DeleteRow, self).__init__(OPERATION_DELETE_ROW)
        self.body = None

    def doit(self, payload):
        row_id = get_row_id(payload)
        row_id, table_id = split_ids(row_id)
        del_res = self.db.delete_row(table_id, row_id)
        st = del_res['ResponseMetadata']['HTTPStatusCode']
        t = (204, "") if st == 200 else (400, "Failed to delete row.")
        return t[0], t[1]