from db import DB
from validator import Validator
from util import ValidationError, msg, decorate_table_response, decorate_columns
from const import OPEARTION_CREATE_TABLE, OPERATION_GET_TABLE_CONFIG, OPERATION_UPDATE_TABLE, schema
import json
import uuid


class TableJob:

    def __init__(self, operation):
        self.db = DB("TableAppDB")
        self.operation = operation

    def doit(self, payload):
        # Implement in all childs
        return 200, {"error": "Operation not implemented."}

    def validate(self, payload):
        # See if operation is registered        
        if self.operation is None:
            print("No such operation found.")
            raise ValidationError("No such operation allowed.")
            # Get if schema contains any validator
        validator_schema = schema.get(self.operation)
        if validator_schema is None:
            # No validator for this operation, return as passed
            return True
        # Assuming that: request body's params need to validate
        # So make sure the request body itself is valid.
        v = Validator(required=True, type=dict)
        status, msg = v.validate(payload)
        if not status:
            # Validation failed.            
            raise ValidationError("body: " + msg)
        for k in validator_schema.keys():
            val = payload.get(k)
            sch = validator_schema[k]
            print("validating key: " + str(k) + " value: " + str(val) + " schema: " + str(sch))
            # Validate
            v = Validator(required=sch.get("required"), type=sch.get("type"),
                          min_length=sch.get("min_length"), max_length=sch.get("max_length"))
            status, msg = v.validate(val)
            # Check the validation status
            if not status:
                # Raise since it fails
                print("key: " + k + " msg: " + str(msg))
                raise ValidationError(k + " : " + msg)

    def get_table_id(self, payload):
        pp = payload.get("pathParameters")
        v = Validator(required=True, type=str)
        table_id = pp.get("id")
        v.validate(table_id)
        return table_id


class CreateTable(TableJob):
    """
    Handle full operation, success and failure msg management, call to DB
    """

    def __init__(self):
        super(CreateTable, self).__init__(OPEARTION_CREATE_TABLE)
        self.body = None

    """
    POST method which create table 
    """

    def doit(self, payload):
        print("Creating new Table.")
        body = payload.get('body')
        body = json.loads(body)
        try:
            self.validate(body)
            columns = decorate_columns(body.pop('columns'))
            body['columns'] = columns
            uid = self.generate_id(body)
            print("Id generated: " + str(uid))
            res, req = self.db.create_table(uid, body)
            status = res["ResponseMetadata"]["HTTPStatusCode"]
            print("Table creation status : " + str(status))
            return status, decorate_table_response(req, status)
        except ValidationError as e:
            return 400, {"error": e.message}

    """
        The function genrates the unique id for table.
        if custom unique name is not available raise a error.
    """

    def generate_id(self, body):
        unique_name = body.get('custom-unique-name')
        print("Checking unique name: " + str(body))
        if unique_name:
            f = self.db.is_table_name_unique(unique_name)
            if f == False:
                raise ValidationError("The given name is not available.")
            else:
                return unique_name
        else:
            return str(uuid.uuid4())


class UpdateTable(TableJob):

    def __init__(self):
        super(UpdateTable, self).__init__(OPERATION_UPDATE_TABLE)
        self.body = None

    def doit(self, payload):
        v = Validator(required=True, type=str)
        table_id = self.get_table_id(payload)
        body = payload.get('body')
        body = json.loads(body)
        try:
            self.validate(body)
        except ValidationError as e:
            return 400, e.message
        item = self.db.get_table_config_item(table_id)
        if item is None:
            return 404, {"error": msg("NOT_FOUND")}
        res, req = self.db.update_table(table_id, body)
        status = res["ResponseMetadata"]["HTTPStatusCode"]
        print("Table update status : " + str(status) + " response: " + str(res))
        attrs = res["Attributes"]
        return status, decorate_table_response(attrs, status)


class DeleteTable(TableJob):
    def __init__(self):
        super(DeleteTable, self).__init__(OPERATION_UPDATE_TABLE)

    def doit(self, payload):
        v = Validator(required=True, type=str)
        table_id = self.get_table_id(payload)
        try:
            v.validate(table_id)
        except ValidationError as e:
            return 400, e.message
        item = self.db.get_table_config_item(table_id)
        if item is None:
            return 404, {"error": msg("NOT_FOUND")}
        del_res = self.db.delete_table(table_id)
        print("Delete Response: " + str(del_res))
        st = del_res['ResponseMetadata']['HTTPStatusCode']
        t = (204, "") if st == 200 else (400, "Failed to delete table.")
        return t[0], t[1]


"""
Get table config. not the data
"""


class GetTableConfig(TableJob):

    def __init__(self):
        super(GetTableConfig, self).__init__(OPERATION_GET_TABLE_CONFIG)

    """
    POST method which create table 
    """

    def doit(self, payload):
        v = Validator(required=True, type=str)
        table_id = self.get_table_id(payload)
        try:
            v.validate(table_id)
        except ValidationError as e:
            return 400, e.message
        res = self.db.get_table_config_by_id(table_id)
        item = res.get("Item")
        print("Get table item: "+str(item))
        if item is None:
            return 404, {"error": msg("NOT_FOUND")}
        item = decorate_table_response(item, 200)
        return 200, item


"""
Check the unique name of the table
TODO write it as REST API when needed.
"""


class CheckUniqueName(TableJob):

    def __init__(self):
        super(GetTableConfig, self).__init__(OPERATION_GET_TABLE_CONFIG)

    """
    POST method which create table 
    """

    def doit(self, payload):
        v = Validator(required=True, type=str)
        table_id = self.get_table_id(payload)
        try:
            v.validate(table_id)
        except ValidationError as e:
            return 400, e.message
        res = self.db.get_table_config_by_id(table_id)
        item = res.get("Item")
        if item is None:
            return 404, {"error": msg("NOT_FOUND")}
        item = decorate_table_response(item, 200)
        return 200, item
