import json
from const import BASE_URL


data = {}
with open('./msg.json') as f:
    data = json.load(f)
msgs = data.get("EN")


def msg(msg_key, *args):
    m = msgs.get(msg_key, msg_key)
    m = m.format(args)
    return m


def decorate_columns(cols):
    decorated_columns = []
    for col in cols:
        if type(col) == str:
            d = {"key": col, "required": False, "type": 'str', "maxLength": '32'}
            decorated_columns.append(d)
        if type(col) == dict:
            d = {}
            if "key" not in col:
                # If no key is given, this can not be a column.
                continue
            d['key'] = col.get('key')
            d['required'] = col.get('required', False)
            d['type'] = col.get('type', 'str')
            d['maxLength'] = str(col.get('maxLength', 32))
            d['minLength'] = str(col.get('minLength', 0))
            decorated_columns.append(d)
    return decorated_columns


def decorate_row_response(data, status):
    if status == 200:
        return data
    else:
        return {"error": "DB error"}


def decorate_table_response(data, status):
    if status == 200:
        res = {
            "name": data.get("name"),
            "description": data.get("description"),
            "id": data.get("pk"),
            "columns": data.get("columns"),
            "uri": BASE_URL + "tables" + "/" + data.get("pk"),
            "createdAt": data.get("createdAt"),
            "updatedAt": data.get("updatedAt")
        }
        return res
    else:
        return {"error": "DB error"}


def convert_str_to_type(str_typ):
    if str_typ == 'str':
        return str
    elif str_typ == 'int':
        return int
    elif str_typ == 'float':
        return float


def get_schema():
    # TODO
    pass


class ValidationError(Exception):
    def __init__(self, msg):
        self.message = msg



