from util import msg


class Validator:

    def __init__(self, required=False, type=None, min_length=None, max_length=None):
        self.required = required
        self.type = type
        self.min_length = min_length
        self.max_length = max_length

    def validate(self, val):
        # Validate required
        f, msg = self.is_required(val)
        if not f:
            return f, msg
        # Validate value type
        f, msg = self.validate_type(val)
        if not f:
            return f, msg
        # Validate min length
        f, msg = self.validate_min_length(val)
        if not f:
            return f, msg
        # Validate max length
        f, msg = self.validate_max_length(val)
        if not f:
            return f, msg
        return True, None

    def is_required(self, val):
        if val is None and self.required:
            print("required validation failed")
            return False, msg("VALUE_IS_REQUIRED")
        return True, None

    def validate_type(self, val):
        if val is None:
            return True, None
        t = type(val)
        if self.type is not None and t != self.type:
            return False, msg("TYPE_MISSMATCH", self.type)
        return True, None

    def validate_min_length(self, val):
        if val is None:
            return True, None
        l = len(val)
        if val is not None and self.min_length is not None and l < self.min_length:
            return False, msg("MIN_LENGTH_FAILED", self.min_length)
        return True, None

    def validate_max_length(self, val):
        if val is None:
            return True, None
        l = len(val)
        if self.max_length is not None and l > self.max_length:
            return False, msg("MAX_LENGTH_FAILED", self.max_length)
        return True, None
