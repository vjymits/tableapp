import util as u


def test_create_row():
    tab_name = u.generate_random_str(20)
    body = {"name": tab_name, "columns": ["TEST1", "#Id"]}
    res = u.create_table(body)
    tid = u.get_table_id(res)
    res = res.json()
    row = u.generate_random_str_values(res['columns'])
    row_res = u.create_row(tid, {"row": row})
    assert row_res.status_code == 200
    row_res = row_res.json()
    rid = row_res.get('id')
    assert rid is not None
    get_res = u.get_row(rid)
    assert get_res.status_code == 200
    u.delete_row(rid)
    u.delete_table(tid)


def test_update_row():
    tab_name = u.generate_random_str(20)
    body = {"name": tab_name, "columns": ["TEST1", "Id"]}
    res = u.create_table(body)
    tid = u.get_table_id(res)
    res = res.json()
    row = u.generate_random_str_values(res['columns'])
    row_res = u.create_row(tid, {"row": row})
    row_res = row_res.json()
    rid = row_res.get('id')
    updated_row_params = u.generate_random_str_values(res['columns'])
    upd_res = u.update_row(rid, {"row": updated_row_params})
    assert upd_res.status_code == 200
    upd_res = upd_res.json()
    print("updated row params: "+str(updated_row_params))
    print("updated row response: "+str(upd_res))
    for k in updated_row_params:
        assert updated_row_params[k] == upd_res[k]
    u.delete_row(rid)
    u.delete_table(tid)


def test_get_row():
    tab_name = u.generate_random_str(20)
    body = {"name": tab_name, "columns": ["TEST1", "Id"]}
    res = u.create_table(body)
    tid = u.get_table_id(res)
    res = res.json()
    row = u.generate_random_str_values(res['columns'])
    row_res = u.create_row(tid, {"row": row})
    row_res = row_res.json()
    rid = row_res.get('id')
    get_res = u.get_row(rid)
    assert get_res.status_code == 200
    get_res = get_res.json()
    for k in row:
        assert row[k] == get_res[k]
    u.delete_row(rid)
    u.delete_table(tid)


def test_delete_row():
    tab_name = u.generate_random_str(20)
    body = {"name": tab_name, "columns": ["TEST1", "#Id"]}
    res = u.create_table(body)
    tid = u.get_table_id(res)
    res = res.json()
    row = u.generate_random_str_values(res['columns'])
    row_res = u.create_row(tid, {"row": row})
    row_res = row_res.json()
    rid = row_res.get('id')
    del_res = u.delete_row(rid)
    assert del_res.status_code == 204
    get_res = u.get_row(rid)
    assert get_res.status_code == 404
    u.delete_table(tid)