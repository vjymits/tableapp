
from tests.rest.util import create_table, update_table, delete_table, get_table, get_table_id
import string
import random


def test_create_table_default():
    letters = string.ascii_uppercase
    rnd_str = ''.join(random.choice(letters) for i in range(10))
    n = "UT-TABLE-TEST-"+rnd_str
    body = {"name": n, "columns": ["TEST1", "Computer Name", "Desc"]}
    resp = create_table(body)
    assert resp.status_code == 200
    resp_body = resp.json()
    assert resp_body['name'] == n
    delete_table(resp_body['id'])


def test_create_table_custom_unique_name():
    letters = string.ascii_uppercase
    rnd_str = ''.join(random.choice(letters) for i in range(10))
    n = "UT-TABLE-TEST-"+rnd_str
    un = 'MY-Name-'+''.join(random.choice(letters) for i in range(6))
    print("custom unique name: "+un)
    body = {"name": n, "columns": ["TEST1", "Computer Name", "Desc"], 'custom-unique-name': un}
    resp = create_table(body)
    uid = get_table_id(resp)
    assert resp.status_code == 200
    resp_body = resp.json()
    assert resp_body['name'] == n
    assert uid == "T:"+un
    response = create_table(body)
    assert response.status_code == 400
    delete_table(uid)


def test_get_table():
    response = create_table()
    uid = get_table_id(response)
    response = get_table(uid)
    assert response.status_code == 200
    delete_table(uid)


def test_update_table():
    response = create_table()
    uid = get_table_id(response)
    letters = string.ascii_uppercase
    rnd_str = ''.join(random.choice(letters) for i in range(10))
    n = "UT-TABLE-TEST-"+rnd_str
    p = ''.join(random.choice(letters) for i in range(10))
    c = ["Name", "Description", "IP", "PROTO", "DNS-1", "DNS-2"]
    email = "test12@example.com"
    body = {"name": n, "columns": c,"password": p, "email":email}
    response = update_table(uid, body)
    assert response.status_code == 200
    inp_column_set = set(c)
    out_column_set = response.json()["columns"]
    assert len(inp_column_set.symmetric_difference(out_column_set)) == 0
    out_id = get_table_id(response)
    assert out_id == uid
    assert response.json()['name'] == n
    delete_table(uid)


def test_delete_table():
    response = create_table()
    uid = get_table_id(response)
    del_response = delete_table(uid)
    assert del_response.status_code == 204
    get_response = get_table(uid)
    assert get_response.status_code == 404

