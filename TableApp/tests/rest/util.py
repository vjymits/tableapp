import json
import random
import string
import requests

base_uri = "http://127.0.0.1:3000"
base_uri="https://xp67422m2i.execute-api.ap-south-1.amazonaws.com/Prod"

headers = {'Content-Type': 'application/json'}


def generate_random_str(n):
    letters = string.ascii_uppercase
    rnd_str = ''.join(random.choice(letters) for i in range(10))
    return rnd_str


def generate_random_str_values(columns):
    row = {}
    for col in columns:
        max = col.get('maxLength', 32)
        min = col.get('minLength', 0)
        n = random.randint(int(min), int(max))
        row[col['key']] = generate_random_str(n)
    return row


def create_table(body=None):
    if body is None:
        letters = string.ascii_uppercase
        rnd_str = ''.join(random.choice(letters) for i in range(10))
        n = "UT-TABLE-TEST-" + rnd_str
        body = {"name": n, "columns": ["Name", "Computer Name", "Desc"]}

    uri = base_uri + '/api/v1/tables'
    # Make REST request
    response = requests.post(uri, headers=headers, data=json.dumps(body, indent=4))
    print("create table response: " + str(response.json()))
    return response


def update_table(uid, body):
    uri = base_uri + '/api/v1/tables/' + uid
    # Make REST request
    response = requests.put(uri, headers=headers, data=json.dumps(body, indent=4))
    print("update table response: " + str(response.json()))
    return response


def delete_table(uid):
    uri = base_uri + '/api/v1/tables/' + uid
    response = requests.delete(uri, headers=headers)
    print("table deleted. ") if response.status_code == 204 else print("Failed to delete table")
    return response


def get_table(uid):
    uri = base_uri + '/api/v1/tables/' + uid
    response = requests.get(uri, headers=headers)
    return response


def get_table_id(response):
    resp_body = response.json()
    uid = resp_body['id']
    return uid


def create_row(table_id, body):
    body['tableId'] = table_id
    uri = base_uri + '/api/v1/rows'
    response = requests.post(uri, headers=headers, data=json.dumps(body, indent=4))
    print("create row response: " + str(response.json()))
    return response


def update_row(row_id, body):
    uri = base_uri + '/api/v1/rows/' + row_id
    # Make REST request
    response = requests.put(uri, headers=headers, data=json.dumps(body, indent=4))
    print("update row response: " + str(response.json()))
    return response


def delete_row(rid):
    uri = base_uri + '/api/v1/rows/' + rid
    response = requests.delete(uri, headers=headers)
    print("row deleted. ") if response.status_code == 204 else print("Failed to delete row")
    return response


def get_row(rid):
    uri = base_uri + '/api/v1/rows/' + rid
    response = requests.get(uri, headers=headers)
    return response
    


