import 'package:flutter/material.dart';
import 'package:tableapp_flutter/constants.dart';
import 'package:tableapp_flutter/screens/new_table.dart';
import 'package:tableapp_flutter/screens/table_view.dart';

import './screens/home.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'TableAPP',
      theme: ThemeData(        
        primarySwatch: primarySwatch,
        primaryColor: primaryColor,
        //scaffoldBackgroundColor:bgColor,
        //canvasColor: secondaryColor,
        
      ),
      initialRoute: '/table',
      routes: {
        '/': (context) => Home(),
        '/new': (context) => NewTable(),
        '/table': (context) => TableView()
      },
      
    );
  }
}

