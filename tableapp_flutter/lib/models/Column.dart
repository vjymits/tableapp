class Column {
  String? type, key;
  bool? required;
  int? minLength, maxLength;
}
