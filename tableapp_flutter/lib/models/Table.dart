import 'package:tableapp_flutter/models/Column.dart';

class MyTable {
  String? id, name, description;
  DateTime? created, updated;
  List<Column>? columns;
}
