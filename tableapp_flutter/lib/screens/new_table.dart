import 'package:flutter/material.dart';
import 'package:tableapp_flutter/constants.dart';
import 'package:tableapp_flutter/responsive.dart';
import './util.dart';

class NewTable extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var appScale = AppScale(context);
    return Scaffold(
      appBar: appBar(),
      body: Center(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
                alignment: Alignment.center,
                width: appScale.scaledWidth(0.40),
                height: appScale.scaledHeight(0.40),
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  elevation: 10,
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text("Table Information needed to create."),
                        TextField(
                          decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Table Name',
                              hintText: 'eg. TODO List'),
                        ),
                        TextField(
                          maxLines: 3,
                          keyboardType: TextInputType.multiline,
                          decoration: InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Description',
                              hintText: 'eg. TODO List'),
                        ),
                      ],
                    ),
                  ),
                )),
            Padding(
              padding: EdgeInsets.all(15.0),
              child: Container(
                width: appScale.scaledWidth(0.40),
                //height: deviceHeight * 0.30,
                alignment: Alignment.center,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  elevation: 10,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Text("Add the columns and Select thier types."),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            bottom: 4.0, top: 4.0, left: 4.0, right: 4.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            SizedBox(
                              width: appScale.scaledWidth(0.15),
                              child: TextField(
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    labelText: 'Column Name',
                                    hintText: 'eg. TO-DO '),
                              ),
                            ),
                            SizedBox(
                                width: appScale.scaledWidth(0.15),
                                child: outLinedDropdown()),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            bottom: 4.0, top: 4.0, left: 4.0, right: 4.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            SizedBox(
                              width: appScale.scaledWidth(0.15),
                              child: TextField(
                                decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                    labelText: 'Column Name',
                                    hintText: 'eg. TO-DO '),
                              ),
                            ),
                            SizedBox(
                                width: appScale.scaledWidth(0.15),
                                child: outLinedDropdown()),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 8, right: 8, top: 8, bottom: 16),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15),
                              child: Container(
                                width: appScale.scaledWidth(0.10),
                                //height: deviceHeight * 0.10,
                                decoration: BoxDecoration(
                                    color: primaryColor,
                                    borderRadius: BorderRadius.circular(20)),
                                child: TextButton(
                                  onPressed: () {},
                                  child: Text(
                                    'Create',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 15),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15),
                              child: Container(
                                width: appScale.scaledWidth(0.10),
                                //height: deviceHeight * 0.5,
                                decoration: BoxDecoration(
                                    color: primaryColor,
                                    borderRadius: BorderRadius.circular(20)),
                                child: TextButton(
                                  onPressed: () {},
                                  child: Text(
                                    'Cancel',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 15),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  DropdownButton getDropdown(List<String> llItems) {
    return DropdownButton<String>(
      items: llItems.map((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
      onChanged: null,
      elevation: 18,
    );
  }
}
