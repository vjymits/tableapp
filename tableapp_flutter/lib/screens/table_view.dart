import 'package:flutter/material.dart';
import 'package:tableapp_flutter/constants.dart';
import 'package:tableapp_flutter/models/Table.dart';
import 'package:tableapp_flutter/responsive.dart';
import 'package:tableapp_flutter/screens/util.dart';

class TableView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new TableState();
  }
}

class TableState extends State<TableView> {
  MyTable? tab;
  String? jsonData;
  var isOpen = false;
  var appScale = null;
  List<String> emails = [];
  var _emailController;

  TableState() {
    // New Table
  }

  @override
  Widget build(BuildContext context) {
    appScale = AppScale(context);
    return Scaffold(
      appBar: tableViewAppBar(),
      body: Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: myTable(),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _emailController = TextEditingController();
  }

  Widget getTitle() {
    return SizedBox(
        width: appScale.scaledWidth(0.4),
        child: TextField(
            decoration: InputDecoration.collapsed(hintText: 'Untitled Table')));
  }

  Widget getDescription() {
    return Container(
      height: appScale.scaledHeight(0.050),
      //width: appScale.scaledWidth(0.50),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.black54))),
      child: Row(
        children: [
          Padding(
            padding: EdgeInsets.only(left: 8, right: 6, bottom: 8, top: 6),
            child: Text("Description: "),
          ),
          Expanded(
            child: TextField(
              decoration: InputDecoration.collapsed(
                  hintText: "Add the description here."),
              textAlign: TextAlign.left,
            ),
          ),
        ],
      ),
    );
  }

  Widget getEmailWidget() {
    return Container(
      alignment: Alignment.bottomLeft,
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.black54))),
      //width: appScale.scaledWidth(0.5),
      child: Row(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 8, right: 63, bottom: 8, top: 6),
            child: Text("To: "),
          ),
          Container(
            constraints: BoxConstraints(
                maxWidth: appScale.scaledWidth(0.9), minWidth: 0),
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  ...emails
                      .map(
                        (email) => InputChip(
                          label: Text(email),
                          deleteIcon: Icon(Icons.close_outlined),
                          avatar: Icon(emails.length == 1
                              ? Icons.admin_panel_settings_rounded
                              : Icons.verified_user_rounded),
                          onDeleted: () {
                            setState(() {
                              emails.remove(email);
                            });
                          },
                        ),
                      )
                      .toList(),
                ],
              ),
            ),
          ),
          Expanded(
            child: TextField(
              decoration: InputDecoration.collapsed(
                  hintText: emails.length > 0
                      ? "  "
                      : "Invite other by email the very first email will be considerd as admin."),
              textAlign: TextAlign.left,
              controller: _emailController,
              onChanged: (String val) {
                if (val.endsWith(' ')) {
                  setState(() {
                    emails.add(_emailController.text);
                    _emailController.text = '';
                  });
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  AppBar tableViewAppBar() {
    return AppBar(
        title: getTitle(),
        backgroundColor: Colors.amber,
        leading: GestureDetector(
          onTap: () {
            setState(() {
              isOpen = !isOpen;
            });
          },
          child: Icon(
            isOpen ? Icons.expand_less_outlined : Icons.expand_more_outlined,
          ),
        ),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: GestureDetector(
              onTap: () {},
              child: TextButton(
                  onPressed: () {},
                  child: Text(
                    "Cancel",
                  )),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: GestureDetector(
              onTap: () {},
              child: TextButton(
                  onPressed: () {},
                  child: Text(
                    "Save",
                  )),
            ),
          ),
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {},
                child: Icon(Icons.more_vert),
              )),
        ],
        bottom: PreferredSize(
          child: isOpen
              ? Container(
                  color: primaryColor,
                  height: 100,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      getEmailWidget(),
                      //Spacer(height: 100),
                      getDescription(),
                      //TextField(),
                    ],
                  ))
              : Container(),
          preferredSize: Size.fromHeight(isOpen ? 100 : 0),
        ));
  }

  Widget myTable() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          DataTable(
            showBottomBorder: true,
            headingRowColor: MaterialStateColor.resolveWith(
              (states) {
                return Colors.black12;
              },
            ),
            columns: <DataColumn>[
              getColumn("Name"),
              getColumn("Phone"),
              getColumn("Email"),
              getColumn("Address"),
              getColumn("Permanent Address"),
              getColumn("Secondary Phone"),
              getColumn("Secondary Email")
            ],
            rows: <DataRow>[
              DataRow(cells: <DataCell>[
                getCell("Shankar"),
                getCell("9019024148"),
                getCell("vjymits@gmail.com"),
                getCell("Lahar road Mihona Bhind MP Pin: 477441"),
                getCell("Lahar road Mihona Bhind MP Pin: 477441"),
                getCell("9019024148"),
                getCell("vjymits@gmail.com"),
              ]),
              DataRow(cells: <DataCell>[
                getCell("Shankar"),
                getCell("9019024148"),
                getCell("vjymits@gmail.com"),
                getCell("Lahar road Mihona Bhind MP Pin: 477441"),
                getCell("Lahar road Mihona Bhind MP Pin: 477441"),
                getCell("9019024148"),
                getCell("vjymits@gmail.com"),
              ]),
              DataRow(cells: <DataCell>[
                getCell("Shankar"),
                getCell("9019024148"),
                getCell("vjymits@gmail.com"),
                getCell("Lahar road Mihona Bhind MP Pin: 477441"),
                getCell("Lahar road Mihona Bhind MP Pin: 477441"),
                getCell("9019024148"),
                getCell("vjymits@gmail.com")
              ]),
              DataRow(cells: <DataCell>[
                getCell("Shankar"),
                getCell("9019024148"),
                getCell("vjymits@gmail.com"),
                getCell("Lahar road Mihona Bhind MP Pin: 477441"),
                getCell("Lahar road Mihona Bhind MP Pin: 477441"),
                getCell("9019024148"),
                getCell("vjymits@gmail.com")
              ]),
              DataRow(cells: <DataCell>[
                getCell("Shankar"),
                getCell("9019024148"),
                getCell("vjymits@gmail.com"),
                getCell("Lahar road Mihona Bhind MP Pin: 477441"),
                getCell("Lahar road Mihona Bhind MP Pin: 477441"),
                getCell("9019024148"),
                getCell("vjymits@gmail.com")
              ])
            ],
          ),
        ],
      ),
    );
  }

  DataColumn getColumn(String col) {
    return DataColumn(
        label: Row(
      children: [
        SizedBox(
            width: appScale.scaledWidth(0.1),
            child: TextField(
              decoration: InputDecoration.collapsed(hintText: ""),
              controller: TextEditingController(text: col),
            )),
        PopupMenuButton(
          icon: Icon(Icons.more_vert),
          itemBuilder: (BuildContext context) => <PopupMenuEntry>[
            const PopupMenuItem(
              child: ListTile(
                leading: Icon(Icons.arrow_right),
                title: Text('Column After'),
              ),
            ),
            const PopupMenuItem(
              child: ListTile(
                leading: Icon(Icons.arrow_left),
                title: Text('Column Before'),
              ),
            ),
            const PopupMenuItem(
              child: ListTile(
                leading: Icon(Icons.delete),
                title: Text('Remove'),
              ),
            ),
            const PopupMenuDivider(),
            const PopupMenuItem(child: Text('Requiered')),
            const PopupMenuItem(child: Text('Type')),
          ],
        ),
      ],
    ));
  }

  DataCell getCell(String val) {
    return DataCell(SizedBox(
        width: appScale.scaledWidth(0.1),
        child: TextField(
          decoration: InputDecoration.collapsed(hintText: val),
          controller: TextEditingController(text: val),
        )));
  }
}
