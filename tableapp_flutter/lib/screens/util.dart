import 'package:flutter/material.dart';

AppBar appBar() {
  return AppBar(
    title: Text("TableApp"),
    leading: GestureDetector(
      onTap: () {/* Write listener code here */},
      child: Icon(
        Icons.menu, // add custom icons also
      ),
    ),
    actions: <Widget>[
      Padding(
          padding: EdgeInsets.only(right: 20.0),
          child: GestureDetector(
            onTap: () {},
            child: Icon(
              Icons.add,
              size: 26.0,
            ),
          )),
      Padding(
          padding: EdgeInsets.only(right: 20.0),
          child: GestureDetector(
            onTap: () {},
            child: Icon(Icons.more_vert),
          )),
    ],
  );
}

Widget outLinedDropdown() {
  String gender= 'Male';
  return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[        
        InputDecorator(
          decoration: InputDecoration(
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
            contentPadding: EdgeInsets.all(10),
          ),
          child: DropdownButtonHideUnderline(
            child: DropdownButton<String>(
              value: gender,
              isDense: true,
              isExpanded: true,
              items: [
                DropdownMenuItem(child: Text("Select Gender"), value: ""),
                DropdownMenuItem(child: Text("Male"), value: "Male"),
                DropdownMenuItem(child: Text("Female"), value: "Female"),
              ],
              onChanged: null,
            ),
          ),
        ),
      ]);
}
